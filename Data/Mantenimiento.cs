﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManApp.Data
{
    class Mantenimiento
    {
        public int MantenimientoId { get; set; }
        public int EquipoId { get; set; }
        public string Descripcion { get; set; }

        //Propiedad de navegacion
        public Equipo equipo { get; set; }
    }
}
