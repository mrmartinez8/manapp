﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManApp.Data
{
    class Cliente
    {
        public int Clienteid { get; set; }
        public string NombreEmpresa { get; set; }
        public string NombreContacto { get; set; }
        public int Telefono { get; set; }
        public List<Equipo> Equipos { get; set; }
    }
}
