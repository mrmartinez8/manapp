﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ManApp.Data
{
    class Equipo
    {
        public int EquipoId { get; set; }
        public int ClienteId { get; set; }
        public string Descripcion { get; set; }
        public string Serial { get; set; }
        public string Marca { get; set; }

        //Propiedad de navegacion
        public Cliente cliente { get; set; }
    }
}
