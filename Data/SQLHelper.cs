﻿using System;
using System.Data;
using System.Data.OleDb;

namespace ManApp.Data
{
    public class SQLDataAccessHelper
    {
        string connectionString = string.Empty;

        public SQLDataAccessHelper()
        {
            try
            {
                connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\MrMartinez\Documents\Developer\ManApp\BD.accdb";

            }
            catch (Exception)
            {
            }
        }

        //INSERT, UPDATE, DELETE
        public bool ExecuteNonQuery(string commandText, CommandType commandType, params OleDbParameter[] commandParameters)
        {
            bool b = false;
            using (var connection = new OleDbConnection(connectionString))
            using (var command = new OleDbCommand(commandText, connection))
            {
                command.CommandType = commandType;
                command.Parameters.AddRange(commandParameters);
                connection.Open();
                int result = command.ExecuteNonQuery();
                if (result == 1)
                {
                    b = true;
                }
                else
                {
                    b = false;
                }
            }
            return b;
        }

        //SELECT
        public DataSet ExecuteQuery(string commandText, CommandType commandType, params OleDbParameter[] parameters)
        {
            using (var connection = new OleDbConnection(connectionString))
            using (var command = new OleDbCommand(commandText, connection))
            {
                DataSet ds = new DataSet();
                command.CommandType = commandType;
                if (parameters != null)
                {
                    command.Parameters.AddRange(parameters);
                }
                OleDbDataAdapter da = new OleDbDataAdapter(command);
                da.Fill(ds);
                connection.Close();
                return ds;
            }
        }


    }
}
