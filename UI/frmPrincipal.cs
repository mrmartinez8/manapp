﻿using ManApp.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManApp.UI
{
    public partial class frmPrincipal : Form
    {
        SQLDataAccessHelper helper = new SQLDataAccessHelper();
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void crearClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCliente frmCliente = new frmCliente();
            frmCliente.Show();
            this.Hide();
        }
        
        private void btn_Click(object sender, EventArgs e)
        {
            if (pMenuLateral.Width==135)
            {
                pMenuLateral.Width = 70;
                btn.Width = 70;
            }
            else
            {
                pMenuLateral.Width = 135;
                btn.Width = 135;
            }
            
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Desea salir de la aplicacion? ", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                Application.Exit();

            }
        }          

        private void realizarMantenimientoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantenimiento frmMantenimiento = new frmMantenimiento();
            this.Hide();
            frmMantenimiento.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmCliente frmCliente = new frmCliente();
            frmCliente.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmEquipo frmEquipo = new frmEquipo();
            frmEquipo.Show();
            this.Hide();
        }

        private void listarClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCliente frmCliente = new frmCliente();
            frmCliente.Show();
            this.Close();
        }

        private void crearClienteToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            frmCliente frmCliente = new frmCliente();
            frmCliente.Show();
            frmCliente.tbClientes.SelectedTab = frmCliente.tbClientes.TabPages[1];
            
            this.Close();
        }

        private void listarEquiposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEquipo equipo = new frmEquipo();
            equipo.Show();
            this.Close();
        }

        private void crearEquipoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEquipo frmEquipo = new frmEquipo();
            frmEquipo.Show();
            frmEquipo.tbEquipos.SelectedTab = frmEquipo.tbEquipos.TabPages[1];
        }

        private void crearMantenimientoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMantenimiento frmEquipo = new frmMantenimiento();
            frmEquipo.Show();
            frmEquipo.tabMantenimiento.SelectedTab = frmEquipo.tabMantenimiento.TabPages[1];
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            LLenarGrid();
        }

        private void LLenarGrid()
        {
            DateTime fInicial = DateTime.Now;
            DateTime fFinal = fInicial.AddDays(7);
            OleDbParameter[] parameters = {
                            new OleDbParameter("@fInicial", fInicial),
                            new OleDbParameter("@fFinal", fFinal)};

            var lista = helper.ExecuteQuery(" SELECT * " +
                                            " FROM vMantenimientos WHERE ProximoMantenimiento  BETWEEN #"+fInicial+"# AND #"+fFinal+"# ", CommandType.Text,null).Tables[0];
            dtPrincipal.DataSource = null;
            dtPrincipal.DataSource = lista;

        }

        private void btnMantenimiento_Click(object sender, EventArgs e)
        {
            frmMantenimiento frmMantenimiento = new frmMantenimiento();
            frmMantenimiento.Show();
            this.Hide();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Desea salir de la aplicacion? ", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                Application.Exit();

            }
        }

        private void dtPrincipal_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            frmMantenimiento frmMantenimiento = new frmMantenimiento();
            int id = (int)dtPrincipal.Rows[e.RowIndex].Cells[0].Value;
            frmMantenimiento.Show();
            this.Hide();
            //frmMantenimiento.tabMantenimiento.Show();
            //frmMantenimiento.estadoBotones();
            frmMantenimiento.llenarGridEquipo(id);
       
            
        }

        private void creditosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCreditos frmCreditos = new frmCreditos();
            frmCreditos.Show();
            
        }
    }
}
