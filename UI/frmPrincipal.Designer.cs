﻿namespace ManApp.UI
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Menu = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listarClientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.equiposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listarEquiposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearEquipoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mantenimientosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.realizarMantenimientoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearMantenimientoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.creditosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pMenuLateral = new System.Windows.Forms.Panel();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnMantenimiento = new System.Windows.Forms.Button();
            this.btnClientes = new System.Windows.Forms.Button();
            this.btnEquipos = new System.Windows.Forms.Button();
            this.pMenuTop = new System.Windows.Forms.FlowLayoutPanel();
            this.btn = new System.Windows.Forms.PictureBox();
            this.pCentral = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.dtPrincipal = new System.Windows.Forms.DataGridView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Menu.SuspendLayout();
            this.pMenuLateral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn)).BeginInit();
            this.pCentral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Menu
            // 
            this.Menu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.clientesToolStripMenuItem,
            this.equiposToolStripMenuItem,
            this.mantenimientosToolStripMenuItem,
            this.ayudaToolStripMenuItem});
            this.Menu.Location = new System.Drawing.Point(0, 0);
            this.Menu.Name = "Menu";
            this.Menu.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.Menu.Size = new System.Drawing.Size(1192, 28);
            this.Menu.TabIndex = 14;
            this.Menu.Text = "menuStrip2";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 24);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(96, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // clientesToolStripMenuItem
            // 
            this.clientesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listarClientesToolStripMenuItem,
            this.crearClienteToolStripMenuItem});
            this.clientesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("clientesToolStripMenuItem.Image")));
            this.clientesToolStripMenuItem.Name = "clientesToolStripMenuItem";
            this.clientesToolStripMenuItem.Size = new System.Drawing.Size(81, 24);
            this.clientesToolStripMenuItem.Text = "Clientes";
            // 
            // listarClientesToolStripMenuItem
            // 
            this.listarClientesToolStripMenuItem.Name = "listarClientesToolStripMenuItem";
            this.listarClientesToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.listarClientesToolStripMenuItem.Text = "Listar Clientes";
            this.listarClientesToolStripMenuItem.Click += new System.EventHandler(this.listarClientesToolStripMenuItem_Click);
            // 
            // crearClienteToolStripMenuItem
            // 
            this.crearClienteToolStripMenuItem.Name = "crearClienteToolStripMenuItem";
            this.crearClienteToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.crearClienteToolStripMenuItem.Text = "Crear Cliente";
            this.crearClienteToolStripMenuItem.Click += new System.EventHandler(this.crearClienteToolStripMenuItem_Click_1);
            // 
            // equiposToolStripMenuItem
            // 
            this.equiposToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listarEquiposToolStripMenuItem,
            this.crearEquipoToolStripMenuItem});
            this.equiposToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("equiposToolStripMenuItem.Image")));
            this.equiposToolStripMenuItem.Name = "equiposToolStripMenuItem";
            this.equiposToolStripMenuItem.Size = new System.Drawing.Size(81, 24);
            this.equiposToolStripMenuItem.Text = "Equipos";
            // 
            // listarEquiposToolStripMenuItem
            // 
            this.listarEquiposToolStripMenuItem.Name = "listarEquiposToolStripMenuItem";
            this.listarEquiposToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.listarEquiposToolStripMenuItem.Text = "Listar Equipos";
            this.listarEquiposToolStripMenuItem.Click += new System.EventHandler(this.listarEquiposToolStripMenuItem_Click);
            // 
            // crearEquipoToolStripMenuItem
            // 
            this.crearEquipoToolStripMenuItem.Name = "crearEquipoToolStripMenuItem";
            this.crearEquipoToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.crearEquipoToolStripMenuItem.Text = "Crear Equipo";
            this.crearEquipoToolStripMenuItem.Click += new System.EventHandler(this.crearEquipoToolStripMenuItem_Click);
            // 
            // mantenimientosToolStripMenuItem
            // 
            this.mantenimientosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.realizarMantenimientoToolStripMenuItem,
            this.crearMantenimientoToolStripMenuItem});
            this.mantenimientosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("mantenimientosToolStripMenuItem.Image")));
            this.mantenimientosToolStripMenuItem.Name = "mantenimientosToolStripMenuItem";
            this.mantenimientosToolStripMenuItem.Size = new System.Drawing.Size(126, 24);
            this.mantenimientosToolStripMenuItem.Text = "Mantenimientos";
            // 
            // realizarMantenimientoToolStripMenuItem
            // 
            this.realizarMantenimientoToolStripMenuItem.Name = "realizarMantenimientoToolStripMenuItem";
            this.realizarMantenimientoToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.realizarMantenimientoToolStripMenuItem.Text = "Listar Mantenimientos";
            this.realizarMantenimientoToolStripMenuItem.Click += new System.EventHandler(this.realizarMantenimientoToolStripMenuItem_Click);
            // 
            // crearMantenimientoToolStripMenuItem
            // 
            this.crearMantenimientoToolStripMenuItem.Name = "crearMantenimientoToolStripMenuItem";
            this.crearMantenimientoToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.crearMantenimientoToolStripMenuItem.Text = "Crear Mantenimiento";
            this.crearMantenimientoToolStripMenuItem.Click += new System.EventHandler(this.crearMantenimientoToolStripMenuItem_Click);
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.creditosToolStripMenuItem});
            this.ayudaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ayudaToolStripMenuItem.Image")));
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(73, 24);
            this.ayudaToolStripMenuItem.Text = "Ayuda";
            // 
            // creditosToolStripMenuItem
            // 
            this.creditosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("creditosToolStripMenuItem.Image")));
            this.creditosToolStripMenuItem.Name = "creditosToolStripMenuItem";
            this.creditosToolStripMenuItem.Size = new System.Drawing.Size(184, 26);
            this.creditosToolStripMenuItem.Text = "Creditos";
            this.creditosToolStripMenuItem.Click += new System.EventHandler(this.creditosToolStripMenuItem_Click);
            // 
            // pMenuLateral
            // 
            this.pMenuLateral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(184)))));
            this.pMenuLateral.Controls.Add(this.btnSalir);
            this.pMenuLateral.Controls.Add(this.btnMantenimiento);
            this.pMenuLateral.Controls.Add(this.btnClientes);
            this.pMenuLateral.Controls.Add(this.btnEquipos);
            this.pMenuLateral.Location = new System.Drawing.Point(0, 69);
            this.pMenuLateral.Margin = new System.Windows.Forms.Padding(2);
            this.pMenuLateral.Name = "pMenuLateral";
            this.pMenuLateral.Size = new System.Drawing.Size(135, 483);
            this.pMenuLateral.TabIndex = 16;
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalir.Location = new System.Drawing.Point(0, 323);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(2);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(154, 76);
            this.btnSalir.TabIndex = 4;
            this.btnSalir.Text = "              Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnMantenimiento
            // 
            this.btnMantenimiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMantenimiento.Image = ((System.Drawing.Image)(resources.GetObject("btnMantenimiento.Image")));
            this.btnMantenimiento.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMantenimiento.Location = new System.Drawing.Point(0, 232);
            this.btnMantenimiento.Margin = new System.Windows.Forms.Padding(2);
            this.btnMantenimiento.Name = "btnMantenimiento";
            this.btnMantenimiento.Size = new System.Drawing.Size(154, 76);
            this.btnMantenimiento.TabIndex = 3;
            this.btnMantenimiento.Text = "              Manten.";
            this.btnMantenimiento.UseVisualStyleBackColor = true;
            this.btnMantenimiento.Click += new System.EventHandler(this.btnMantenimiento_Click);
            // 
            // btnClientes
            // 
            this.btnClientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClientes.Image = ((System.Drawing.Image)(resources.GetObject("btnClientes.Image")));
            this.btnClientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClientes.Location = new System.Drawing.Point(0, 46);
            this.btnClientes.Margin = new System.Windows.Forms.Padding(2);
            this.btnClientes.Name = "btnClientes";
            this.btnClientes.Size = new System.Drawing.Size(154, 76);
            this.btnClientes.TabIndex = 1;
            this.btnClientes.Text = "              Clientes";
            this.btnClientes.UseVisualStyleBackColor = true;
            this.btnClientes.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnEquipos
            // 
            this.btnEquipos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEquipos.Image = ((System.Drawing.Image)(resources.GetObject("btnEquipos.Image")));
            this.btnEquipos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEquipos.Location = new System.Drawing.Point(0, 139);
            this.btnEquipos.Margin = new System.Windows.Forms.Padding(2);
            this.btnEquipos.Name = "btnEquipos";
            this.btnEquipos.Size = new System.Drawing.Size(154, 76);
            this.btnEquipos.TabIndex = 2;
            this.btnEquipos.Text = "             Equipos";
            this.btnEquipos.UseVisualStyleBackColor = true;
            this.btnEquipos.Click += new System.EventHandler(this.button2_Click);
            // 
            // pMenuTop
            // 
            this.pMenuTop.BackColor = System.Drawing.Color.White;
            this.pMenuTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pMenuTop.Location = new System.Drawing.Point(0, 28);
            this.pMenuTop.Margin = new System.Windows.Forms.Padding(2);
            this.pMenuTop.Name = "pMenuTop";
            this.pMenuTop.Size = new System.Drawing.Size(1192, 12);
            this.pMenuTop.TabIndex = 17;
            // 
            // btn
            // 
            this.btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btn.Image = ((System.Drawing.Image)(resources.GetObject("btn.Image")));
            this.btn.Location = new System.Drawing.Point(0, 44);
            this.btn.Margin = new System.Windows.Forms.Padding(2);
            this.btn.Name = "btn";
            this.btn.Size = new System.Drawing.Size(135, 28);
            this.btn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn.TabIndex = 0;
            this.btn.TabStop = false;
            this.btn.Click += new System.EventHandler(this.btn_Click);
            // 
            // pCentral
            // 
            this.pCentral.BackColor = System.Drawing.Color.White;
            this.pCentral.Controls.Add(this.label1);
            this.pCentral.Controls.Add(this.dtPrincipal);
            this.pCentral.Controls.Add(this.pictureBox1);
            this.pCentral.Location = new System.Drawing.Point(68, 44);
            this.pCentral.Margin = new System.Windows.Forms.Padding(2);
            this.pCentral.Name = "pCentral";
            this.pCentral.Size = new System.Drawing.Size(1125, 506);
            this.pCentral.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(120, 164);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(254, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Mantenimientos para esta semana";
            // 
            // dtPrincipal
            // 
            this.dtPrincipal.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtPrincipal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtPrincipal.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtPrincipal.Location = new System.Drawing.Point(118, 186);
            this.dtPrincipal.Margin = new System.Windows.Forms.Padding(2);
            this.dtPrincipal.MultiSelect = false;
            this.dtPrincipal.Name = "dtPrincipal";
            this.dtPrincipal.RowTemplate.Height = 24;
            this.dtPrincipal.Size = new System.Drawing.Size(957, 280);
            this.dtPrincipal.TabIndex = 2;
            this.dtPrincipal.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtPrincipal_CellClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(470, 13);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(277, 150);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1192, 552);
            this.Controls.Add(this.pMenuLateral);
            this.Controls.Add(this.btn);
            this.Controls.Add(this.pMenuTop);
            this.Controls.Add(this.Menu);
            this.Controls.Add(this.pCentral);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.Menu;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ManApp";
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            this.Menu.ResumeLayout(false);
            this.Menu.PerformLayout();
            this.pMenuLateral.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn)).EndInit();
            this.pCentral.ResumeLayout(false);
            this.pCentral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip Menu;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem equiposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mantenimientosToolStripMenuItem;
        private System.Windows.Forms.Panel pMenuLateral;
        private System.Windows.Forms.FlowLayoutPanel pMenuTop;
        private System.Windows.Forms.Panel pCentral;
        private System.Windows.Forms.PictureBox btn;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem listarClientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listarEquiposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem realizarMantenimientoToolStripMenuItem;
        private System.Windows.Forms.Button btnEquipos;
        private System.Windows.Forms.Button btnClientes;
        private System.Windows.Forms.DataGridView dtPrincipal;
        private System.Windows.Forms.Button btnMantenimiento;
        private System.Windows.Forms.ToolStripMenuItem crearClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearEquipoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearMantenimientoToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem creditosToolStripMenuItem;
    }
}