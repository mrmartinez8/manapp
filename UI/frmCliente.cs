﻿using ManApp.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ManApp.UI;


namespace ManApp
{
    public partial class frmCliente : Form
    {
        SQLDataAccessHelper helper = new SQLDataAccessHelper();

        OleDbConnection con = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\MrMartinez\Documents\Developer\ManApp\BD.accdb");
        public frmCliente()
        {
            InitializeComponent();
        }

        private void frmCliente_Load(object sender, EventArgs e)
        {
            llenarGrid();
            estadoBotones();

        }

        private void estadoBotones()
        {
            btnCrear.Enabled = false;
            btnLimpiar.Enabled = false;
            btnEditar.Enabled = false;
            if (txtID.Text !="")
            {
                btnCrear.Enabled = false;
                
            }
            else
            {
                btnEditar.Enabled = false;
            }
        }

        private void llenarGrid()
        {
            var lista = helper.ExecuteQuery("select * from Empresas", CommandType.Text, null).Tables[0];

            dtClientes.DataSource = null;
            dtClientes.DataSource = lista;

            #region Codigo anterior
            //con.Open();
            //OleDbCommand cmd = con.CreateCommand();
            //cmd.CommandType = CommandType.Text;
            //cmd.CommandText = "select * from Empresas";
            //cmd.ExecuteNonQuery();

            ////Crear la tabla
            //DataTable dt = new DataTable();
            //OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            //da.Fill(dt);
            //dtClientes.DataSource = null;
            //dtClientes.DataSource = dt;
            //con.Close();
            #endregion
        }

       

        private void btnCrear_Click(object sender, EventArgs e)
        {
            /*Data.Cliente cliente = new Data.Cliente();
            cliente.NombreEmpresa = NombreEmpresa.Text;
            cliente.NombreContacto = NombreContacto.Text;
            cliente.Telefono =Convert.ToInt32(txtTelefono.Text);*/

            try
            {
                if (txtNombre.Text != "")
                {
                    OleDbParameter[] parameters = {
                            new OleDbParameter("@Nombre", txtNombre.Text),
                            new OleDbParameter("@Contacto", txtContacto.Text),
                            new OleDbParameter("@Telefono", txtTelefono.Text),
                            new OleDbParameter("@Telefono", txtEmail.Text),
                            new OleDbParameter("@Celular", txtCelular.Text),
                            new OleDbParameter("@Direccion", txtDireccion.Text),
                            new OleDbParameter("@Posicion",txtPosicion.Text),
                            new OleDbParameter("@Fecha", DateTime.Now.ToShortDateString()),

                    };
                  helper.ExecuteNonQuery("INSERT INTO Empresas(Nombre, Contacto, Telefono, Email, Celular, Direccion, Posicion, FechaIngreso) VALUES(@Nombre, @Contacto, @Telefono, @Email, @Celular, @Direccion, @Posicion,@Fecha)", CommandType.Text, parameters);
                    //con.Open();
                    //OleDbCommand cmd = con.CreateCommand();
                    //cmd.CommandType = CommandType.Text;
                    //cmd.CommandText = "INSERT INTO Clientes(Empresa, Contacto, Telefono, Celular, Direccion, Posicion,FechaIngreso) " +
                    //                  "              VALUES(@Empresa, @Contacto, @Telefono, @Celular, @Direccion, @Posicion,@Fecha)";
                    //cmd.Parameters.AddWithValue("@Empresa", txtEmpresa.Text);
                    //cmd.Parameters.AddWithValue("@Contacto", txtContacto.Text);
                    //cmd.Parameters.AddWithValue("@Telefono", txtTelefono.Text);
                    //cmd.Parameters.AddWithValue("@Celular", txtCelular.Text);
                    //cmd.Parameters.AddWithValue("@Direccion", txtDireccion.Text);
                    //cmd.Parameters.AddWithValue("@Posicion",txtPosicion.Text);
                    //cmd.Parameters.AddWithValue("@Fecha", DateTime.Now);

                    //cmd.ExecuteNonQuery();
                    //con.Close();
                    MessageBox.Show("Guardo con exito");
                    limpiarCampos();
                    llenarGrid();
                    tbClientes.SelectedTab=tabListado;
                }
                else
                {
                    MessageBox.Show("Falta el nombre de la empresa");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error guardando los datos" + ex);
            }
        }

        private void limpiarCampos()
        {
            txtID.Text = "";
            txtNombre.Text = "";
            txtTelefono.Text = "";
            txtEmail.Text = "";
            txtDireccion.Text = "";
            txtContacto.Text = "";
            txtCelular.Text = "";
            txtPosicion.Text = "";
        }
        
        private void txtEmpresa_KeyDown(object sender, KeyEventArgs e)
        {

            if (txtNombre.Text != null)
            {
                if (txtID.Text != "")
                {
                    btnCrear.Enabled = false;
                }
                else
                {
                    btnLimpiar.Enabled = true;
                    btnCrear.Enabled = true;
                }
               

            }
            else 
            {
                btnLimpiar.Enabled = false;
                btnCrear.Enabled = false;
            }

        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            limpiarCampos();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            frmPrincipal frmPrincipal = new frmPrincipal();
            this.Close();
            frmPrincipal.Show();
        }    

        private void dtClientes_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {

            int id = (int)dtClientes.Rows[e.RowIndex].Cells[0].Value;
            LlenarCampos(id);
            btnEditar.Enabled = true;
            btnLimpiar.Enabled = true;
         
            //tbClientes.SelectedIndex = 1;
            tbClientes.SelectedTab = tabCrear;
            txtNombre.Focus();
        }
        private void LlenarCampos(int id)
        {

            DataTable consulta = helper.ExecuteQuery("SELECT * FROM Empresas WHERE EmpresaId = " + id + " ", CommandType.Text, null).Tables[0];
            txtID.Text = consulta.Rows[0][0].ToString();
            txtNombre.Text = consulta.Rows[0][1].ToString();
            txtContacto.Text = consulta.Rows[0][2].ToString();
            txtTelefono.Text = consulta.Rows[0][3].ToString();
            txtCelular.Text = consulta.Rows[0][4].ToString();
            txtEmail.Text = consulta.Rows[0][5].ToString();
            txtDireccion.Text = consulta.Rows[0][6].ToString();
            txtPosicion.Text = consulta.Rows[0][7].ToString();


        }
        private void Editar()
        {
            try
            {
             
                if (txtNombre.Text != "")
                {
                    btnCrear.Enabled = false;
                    //OleDbParameter[] parameters = {
                    //        new OleDbParameter("@ID", Convert.ToInt32(txtID.Text)),
                    //        new OleDbParameter("@Nombre", txtEmpresa.Text),
                    //        new OleDbParameter("@Contacto", txtContacto.Text),
                    //        new OleDbParameter("@Telefono", txtTelefono.Text),
                    //        new OleDbParameter("@Celular", txtCelular.Text),
                    //        new OleDbParameter("@Direccion", txtDireccion.Text),
                    //        new OleDbParameter("@Posicion",txtPosicion.Text),
                    //        new OleDbParameter("@Fecha", DateTime.Now.ToLongDateString()),

                    //};

                    //var resultado = helper.ExecuteNonQuery("UPDATE Empresas SET Nombre = @Nombre, Contacto = @Contacto, Celular = @Celular," +
                    //                                          " Direccion = @Direccion, Posicion = @Posicion, FechaIngreso = @Fecha " +
                    //                                          "WHERE EmpresaId = @ID", CommandType.Text, parameters);



                    con.Open();
                    OleDbCommand cmd = con.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@ID", Convert.ToInt32(txtID.Text));
                    cmd.Parameters.AddWithValue("@Nombre", txtNombre.Text);
                    cmd.CommandText = @"UPDATE Empresas SET Nombre = ' "+txtNombre.Text+ " ', " +
                                                            "Contacto = ' " + txtContacto.Text + "'," +
                                                            "Telefono = '"+txtTelefono.Text+"', " +
                                                            "Celular = '" + txtCelular.Text + "', " +
                                                            "Email = '" + txtEmail.Text + "', " +
                                                            "Direccion = '"+txtDireccion.Text+"', " +
                                                            "Posicion = '"+txtPosicion.Text+"' " +
                                                            "WHERE EmpresaId = @ID";
                    
                    //cmd.Parameters.AddWithValue("@Contacto", txtContacto.Text);
                    //cmd.Parameters.AddWithValue("@Telefono", txtTelefono.Text);
                    //cmd.Parameters.AddWithValue("@Celular", txtCelular.Text);
                    //cmd.Parameters.AddWithValue("@Direccion", txtDireccion.Text);
                    //cmd.Parameters.AddWithValue("@Posicion", txtPosicion.Text);
                    //cmd.Parameters.AddWithValue("@Fecha", DateTime.Now);
                    cmd.ExecuteNonQuery();
                    con.Close();


                    MessageBox.Show("Registro Modificado");
                    limpiarCampos();
                    llenarGrid();
                    tbClientes.SelectedTab = tabListado;




                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Error modificando los datos" + ex);
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            Editar();
        }
    }
}
