﻿namespace ManApp.UI
{
    partial class frmEquipo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEquipo));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnVolver = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnCrearEquipo = new System.Windows.Forms.Button();
            this.btnEditarEquipo = new System.Windows.Forms.Button();
            this.tbEquipos = new System.Windows.Forms.TabControl();
            this.tabListado = new System.Windows.Forms.TabPage();
            this.dtEquipos = new System.Windows.Forms.DataGridView();
            this.tabCrear = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.txtConexion = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtHZ = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtAmperes = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtVoltios = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtKW = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtGenerador = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtHP = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRPM = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMotor = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.cbClientes = new System.Windows.Forms.ComboBox();
            this.txtMarca = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSerial = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.Menu = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listarClientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.equiposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listarEquiposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearEquipoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mantenimientosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.realizarMantenimientoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearMantenimientoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tbEquipos.SuspendLayout();
            this.tabListado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtEquipos)).BeginInit();
            this.tabCrear.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.Menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnVolver
            // 
            this.btnVolver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVolver.Image = ((System.Drawing.Image)(resources.GetObject("btnVolver.Image")));
            this.btnVolver.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVolver.Location = new System.Drawing.Point(1070, 285);
            this.btnVolver.Margin = new System.Windows.Forms.Padding(2);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(102, 75);
            this.btnVolver.TabIndex = 18;
            this.btnVolver.Text = "Volver";
            this.btnVolver.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLimpiar.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpiar.Image")));
            this.btnLimpiar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpiar.Location = new System.Drawing.Point(1070, 67);
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(2);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(102, 75);
            this.btnLimpiar.TabIndex = 17;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnCrearEquipo
            // 
            this.btnCrearEquipo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCrearEquipo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCrearEquipo.Image = ((System.Drawing.Image)(resources.GetObject("btnCrearEquipo.Image")));
            this.btnCrearEquipo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCrearEquipo.Location = new System.Drawing.Point(1070, 140);
            this.btnCrearEquipo.Margin = new System.Windows.Forms.Padding(2);
            this.btnCrearEquipo.Name = "btnCrearEquipo";
            this.btnCrearEquipo.Size = new System.Drawing.Size(102, 75);
            this.btnCrearEquipo.TabIndex = 16;
            this.btnCrearEquipo.Text = "Crear";
            this.btnCrearEquipo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCrearEquipo.UseVisualStyleBackColor = true;
            this.btnCrearEquipo.Click += new System.EventHandler(this.btnCrearEquipo_Click);
            // 
            // btnEditarEquipo
            // 
            this.btnEditarEquipo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditarEquipo.Image = ((System.Drawing.Image)(resources.GetObject("btnEditarEquipo.Image")));
            this.btnEditarEquipo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEditarEquipo.Location = new System.Drawing.Point(1070, 212);
            this.btnEditarEquipo.Margin = new System.Windows.Forms.Padding(2);
            this.btnEditarEquipo.Name = "btnEditarEquipo";
            this.btnEditarEquipo.Size = new System.Drawing.Size(102, 75);
            this.btnEditarEquipo.TabIndex = 19;
            this.btnEditarEquipo.Text = "Editar";
            this.btnEditarEquipo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEditarEquipo.UseVisualStyleBackColor = true;
            this.btnEditarEquipo.Click += new System.EventHandler(this.btnEditarEquipo_Click);
            // 
            // tbEquipos
            // 
            this.tbEquipos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEquipos.Controls.Add(this.tabListado);
            this.tbEquipos.Controls.Add(this.tabCrear);
            this.tbEquipos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbEquipos.Location = new System.Drawing.Point(9, 46);
            this.tbEquipos.Margin = new System.Windows.Forms.Padding(2);
            this.tbEquipos.Name = "tbEquipos";
            this.tbEquipos.SelectedIndex = 0;
            this.tbEquipos.Size = new System.Drawing.Size(1057, 452);
            this.tbEquipos.TabIndex = 20;
            // 
            // tabListado
            // 
            this.tabListado.Controls.Add(this.dtEquipos);
            this.tabListado.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabListado.Location = new System.Drawing.Point(4, 29);
            this.tabListado.Margin = new System.Windows.Forms.Padding(2);
            this.tabListado.Name = "tabListado";
            this.tabListado.Padding = new System.Windows.Forms.Padding(2);
            this.tabListado.Size = new System.Drawing.Size(1049, 419);
            this.tabListado.TabIndex = 0;
            this.tabListado.Text = "Listado";
            this.tabListado.UseVisualStyleBackColor = true;
            // 
            // dtEquipos
            // 
            this.dtEquipos.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSkyBlue;
            this.dtEquipos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtEquipos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtEquipos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtEquipos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtEquipos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtEquipos.Location = new System.Drawing.Point(2, 2);
            this.dtEquipos.Margin = new System.Windows.Forms.Padding(2);
            this.dtEquipos.Name = "dtEquipos";
            this.dtEquipos.RowTemplate.Height = 24;
            this.dtEquipos.Size = new System.Drawing.Size(1044, 403);
            this.dtEquipos.TabIndex = 2;
            this.dtEquipos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtEquipos_CellClick);
            // 
            // tabCrear
            // 
            this.tabCrear.Controls.Add(this.pictureBox1);
            this.tabCrear.Controls.Add(this.label15);
            this.tabCrear.Controls.Add(this.panel1);
            this.tabCrear.Controls.Add(this.label7);
            this.tabCrear.Controls.Add(this.panel2);
            this.tabCrear.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabCrear.Location = new System.Drawing.Point(4, 29);
            this.tabCrear.Margin = new System.Windows.Forms.Padding(2);
            this.tabCrear.Name = "tabCrear";
            this.tabCrear.Padding = new System.Windows.Forms.Padding(2);
            this.tabCrear.Size = new System.Drawing.Size(1049, 419);
            this.tabCrear.TabIndex = 1;
            this.tabCrear.Text = "Crear / Modificar";
            this.tabCrear.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(14, 77);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(134, 223);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 34;
            this.pictureBox1.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(506, 15);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(110, 17);
            this.label15.TabIndex = 33;
            this.label15.Text = "Datos Tecnicos:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.txtConexion);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.txtHZ);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.txtAmperes);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.txtVoltios);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.txtKW);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.txtGenerador);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txtHP);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtRPM);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtMotor);
            this.panel1.Location = new System.Drawing.Point(510, 38);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(441, 331);
            this.panel1.TabIndex = 32;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(239, 207);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 17);
            this.label13.TabIndex = 64;
            this.label13.Text = "CONEXION:";
            // 
            // txtConexion
            // 
            this.txtConexion.Location = new System.Drawing.Point(344, 204);
            this.txtConexion.Margin = new System.Windows.Forms.Padding(2);
            this.txtConexion.Name = "txtConexion";
            this.txtConexion.Size = new System.Drawing.Size(86, 23);
            this.txtConexion.TabIndex = 8;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(300, 151);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 17);
            this.label14.TabIndex = 62;
            this.label14.Text = "HZ:";
            // 
            // txtHZ
            // 
            this.txtHZ.Location = new System.Drawing.Point(344, 147);
            this.txtHZ.Margin = new System.Windows.Forms.Padding(2);
            this.txtHZ.Name = "txtHZ";
            this.txtHZ.Size = new System.Drawing.Size(86, 23);
            this.txtHZ.TabIndex = 7;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(309, 95);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 17);
            this.label12.TabIndex = 60;
            this.label12.Text = "A:";
            // 
            // txtAmperes
            // 
            this.txtAmperes.Location = new System.Drawing.Point(344, 90);
            this.txtAmperes.Margin = new System.Windows.Forms.Padding(2);
            this.txtAmperes.Name = "txtAmperes";
            this.txtAmperes.Size = new System.Drawing.Size(86, 23);
            this.txtAmperes.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(309, 39);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 17);
            this.label9.TabIndex = 58;
            this.label9.Text = "V:";
            // 
            // txtVoltios
            // 
            this.txtVoltios.Location = new System.Drawing.Point(344, 33);
            this.txtVoltios.Margin = new System.Windows.Forms.Padding(2);
            this.txtVoltios.Name = "txtVoltios";
            this.txtVoltios.Size = new System.Drawing.Size(86, 23);
            this.txtVoltios.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(70, 260);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 17);
            this.label10.TabIndex = 56;
            this.label10.Text = "KW:";
            // 
            // txtKW
            // 
            this.txtKW.Location = new System.Drawing.Point(123, 259);
            this.txtKW.Margin = new System.Windows.Forms.Padding(2);
            this.txtKW.Name = "txtKW";
            this.txtKW.Size = new System.Drawing.Size(93, 23);
            this.txtKW.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(2, 204);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(101, 17);
            this.label11.TabIndex = 54;
            this.label11.Text = "GENERADOR:";
            // 
            // txtGenerador
            // 
            this.txtGenerador.Location = new System.Drawing.Point(123, 202);
            this.txtGenerador.Margin = new System.Windows.Forms.Padding(2);
            this.txtGenerador.Name = "txtGenerador";
            this.txtGenerador.Size = new System.Drawing.Size(93, 23);
            this.txtGenerador.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(76, 148);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 17);
            this.label8.TabIndex = 52;
            this.label8.Text = "HP:";
            // 
            // txtHP
            // 
            this.txtHP.Location = new System.Drawing.Point(123, 145);
            this.txtHP.Margin = new System.Windows.Forms.Padding(2);
            this.txtHP.Name = "txtHP";
            this.txtHP.Size = new System.Drawing.Size(93, 23);
            this.txtHP.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(64, 92);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 17);
            this.label4.TabIndex = 50;
            this.label4.Text = "RPM:";
            // 
            // txtRPM
            // 
            this.txtRPM.Location = new System.Drawing.Point(123, 89);
            this.txtRPM.Margin = new System.Windows.Forms.Padding(2);
            this.txtRPM.Name = "txtRPM";
            this.txtRPM.Size = new System.Drawing.Size(93, 23);
            this.txtRPM.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 36);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 48;
            this.label2.Text = "MOTOR:";
            // 
            // txtMotor
            // 
            this.txtMotor.Location = new System.Drawing.Point(123, 32);
            this.txtMotor.Margin = new System.Windows.Forms.Padding(2);
            this.txtMotor.Name = "txtMotor";
            this.txtMotor.Size = new System.Drawing.Size(93, 23);
            this.txtMotor.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(150, 15);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 17);
            this.label7.TabIndex = 27;
            this.label7.Text = "Datos del Equipo:";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.txtID);
            this.panel2.Controls.Add(this.cbClientes);
            this.panel2.Controls.Add(this.txtMarca);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.txtSerial);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtDescripcion);
            this.panel2.Location = new System.Drawing.Point(154, 38);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(351, 331);
            this.panel2.TabIndex = 31;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(46, 16);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 17);
            this.label16.TabIndex = 30;
            this.label16.Text = "CODIGO:";
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(134, 14);
            this.txtID.Margin = new System.Windows.Forms.Padding(2);
            this.txtID.Name = "txtID";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(44, 23);
            this.txtID.TabIndex = 29;
            this.txtID.TabStop = false;
            // 
            // cbClientes
            // 
            this.cbClientes.FormattingEnabled = true;
            this.cbClientes.Location = new System.Drawing.Point(134, 253);
            this.cbClientes.Margin = new System.Windows.Forms.Padding(2);
            this.cbClientes.Name = "cbClientes";
            this.cbClientes.Size = new System.Drawing.Size(163, 24);
            this.cbClientes.TabIndex = 3;
            // 
            // txtMarca
            // 
            this.txtMarca.Location = new System.Drawing.Point(134, 97);
            this.txtMarca.Margin = new System.Windows.Forms.Padding(2);
            this.txtMarca.Name = "txtMarca";
            this.txtMarca.Size = new System.Drawing.Size(163, 23);
            this.txtMarca.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(64, 176);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 17);
            this.label6.TabIndex = 26;
            this.label6.Text = "SERIE:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 253);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 17);
            this.label5.TabIndex = 22;
            this.label5.Text = "PROPIETARIO:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 98);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 17);
            this.label3.TabIndex = 18;
            this.label3.Text = "MARCA:";
            // 
            // txtSerial
            // 
            this.txtSerial.Location = new System.Drawing.Point(134, 175);
            this.txtSerial.Margin = new System.Windows.Forms.Padding(2);
            this.txtSerial.Name = "txtSerial";
            this.txtSerial.Size = new System.Drawing.Size(163, 23);
            this.txtSerial.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 49);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 17);
            this.label1.TabIndex = 14;
            this.label1.Text = "DESCRIPCION:";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(134, 46);
            this.txtDescripcion.Margin = new System.Windows.Forms.Padding(2);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(163, 23);
            this.txtDescripcion.TabIndex = 0;
            // 
            // Menu
            // 
            this.Menu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.clientesToolStripMenuItem,
            this.equiposToolStripMenuItem,
            this.mantenimientosToolStripMenuItem});
            this.Menu.Location = new System.Drawing.Point(0, 0);
            this.Menu.Name = "Menu";
            this.Menu.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.Menu.Size = new System.Drawing.Size(1179, 28);
            this.Menu.TabIndex = 21;
            this.Menu.Text = "menuStrip2";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 24);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(96, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            // 
            // clientesToolStripMenuItem
            // 
            this.clientesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listarClientesToolStripMenuItem,
            this.crearClienteToolStripMenuItem});
            this.clientesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("clientesToolStripMenuItem.Image")));
            this.clientesToolStripMenuItem.Name = "clientesToolStripMenuItem";
            this.clientesToolStripMenuItem.Size = new System.Drawing.Size(81, 24);
            this.clientesToolStripMenuItem.Text = "Clientes";
            // 
            // listarClientesToolStripMenuItem
            // 
            this.listarClientesToolStripMenuItem.Name = "listarClientesToolStripMenuItem";
            this.listarClientesToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.listarClientesToolStripMenuItem.Text = "Listar Clientes";
            // 
            // crearClienteToolStripMenuItem
            // 
            this.crearClienteToolStripMenuItem.Name = "crearClienteToolStripMenuItem";
            this.crearClienteToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.crearClienteToolStripMenuItem.Text = "Crear Cliente";
            // 
            // equiposToolStripMenuItem
            // 
            this.equiposToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listarEquiposToolStripMenuItem,
            this.crearEquipoToolStripMenuItem});
            this.equiposToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("equiposToolStripMenuItem.Image")));
            this.equiposToolStripMenuItem.Name = "equiposToolStripMenuItem";
            this.equiposToolStripMenuItem.Size = new System.Drawing.Size(81, 24);
            this.equiposToolStripMenuItem.Text = "Equipos";
            // 
            // listarEquiposToolStripMenuItem
            // 
            this.listarEquiposToolStripMenuItem.Name = "listarEquiposToolStripMenuItem";
            this.listarEquiposToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.listarEquiposToolStripMenuItem.Text = "Listar Equipos";
            // 
            // crearEquipoToolStripMenuItem
            // 
            this.crearEquipoToolStripMenuItem.Name = "crearEquipoToolStripMenuItem";
            this.crearEquipoToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.crearEquipoToolStripMenuItem.Text = "Crear Equipo";
            // 
            // mantenimientosToolStripMenuItem
            // 
            this.mantenimientosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.realizarMantenimientoToolStripMenuItem,
            this.crearMantenimientoToolStripMenuItem});
            this.mantenimientosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("mantenimientosToolStripMenuItem.Image")));
            this.mantenimientosToolStripMenuItem.Name = "mantenimientosToolStripMenuItem";
            this.mantenimientosToolStripMenuItem.Size = new System.Drawing.Size(126, 24);
            this.mantenimientosToolStripMenuItem.Text = "Mantenimientos";
            // 
            // realizarMantenimientoToolStripMenuItem
            // 
            this.realizarMantenimientoToolStripMenuItem.Name = "realizarMantenimientoToolStripMenuItem";
            this.realizarMantenimientoToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.realizarMantenimientoToolStripMenuItem.Text = "Listar Mantenimientos";
            // 
            // crearMantenimientoToolStripMenuItem
            // 
            this.crearMantenimientoToolStripMenuItem.Name = "crearMantenimientoToolStripMenuItem";
            this.crearMantenimientoToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.crearMantenimientoToolStripMenuItem.Text = "Crear Mantenimiento";
            // 
            // frmEquipo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1179, 514);
            this.Controls.Add(this.Menu);
            this.Controls.Add(this.tbEquipos);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btnCrearEquipo);
            this.Controls.Add(this.btnEditarEquipo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmEquipo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Equipos";
            this.Load += new System.EventHandler(this.frmEquipo_Load);
            this.tbEquipos.ResumeLayout(false);
            this.tabListado.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtEquipos)).EndInit();
            this.tabCrear.ResumeLayout(false);
            this.tabCrear.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.Menu.ResumeLayout(false);
            this.Menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnCrearEquipo;
        private System.Windows.Forms.Button btnEditarEquipo;
        public System.Windows.Forms.TabControl tbEquipos;
        public System.Windows.Forms.TabPage tabListado;
        private System.Windows.Forms.DataGridView dtEquipos;
        public System.Windows.Forms.TabPage tabCrear;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cbClientes;
        private System.Windows.Forms.TextBox txtMarca;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSerial;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtConexion;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtHZ;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtAmperes;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtVoltios;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtKW;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtGenerador;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtHP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtRPM;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMotor;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.MenuStrip Menu;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listarClientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem equiposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listarEquiposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearEquipoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mantenimientosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem realizarMantenimientoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearMantenimientoToolStripMenuItem;
    }
}