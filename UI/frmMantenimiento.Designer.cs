﻿namespace ManApp.UI
{
    partial class frmMantenimiento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMantenimiento));
            this.btnEditarEquipo = new System.Windows.Forms.Button();
            this.btnCrearEquipo = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnVolver = new System.Windows.Forms.Button();
            this.tabCrear = new System.Windows.Forms.TabPage();
            this.txtHoras = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lbFecha = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtEquipoId = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.txtInformacionAdicional = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.cProximoMantenimiento = new System.Windows.Forms.MonthCalendar();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.rbCorreaBombaAgua1B = new System.Windows.Forms.RadioButton();
            this.rbCorreaBombaAgua1A = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.rbCorreaAlternador1B = new System.Windows.Forms.RadioButton();
            this.rbCorreaAlternador1A = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.rbAceite1B = new System.Windows.Forms.RadioButton();
            this.rbAceite1A = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rbCooland1B = new System.Windows.Forms.RadioButton();
            this.rbCooland1A = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.rbFiltroAgua1B = new System.Windows.Forms.RadioButton();
            this.rbFiltroAgua1A = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbFiltroAire1B = new System.Windows.Forms.RadioButton();
            this.rbFiltroAire1A = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbFiltroGasoil1B = new System.Windows.Forms.RadioButton();
            this.rbFiltroGasoil1A = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbFiltroAceite1B = new System.Windows.Forms.RadioButton();
            this.rbFiltroAceite1A = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbEquipo = new System.Windows.Forms.ComboBox();
            this.cbCliente = new System.Windows.Forms.ComboBox();
            this.tabListado = new System.Windows.Forms.TabPage();
            this.dtMantenimientos = new System.Windows.Forms.DataGridView();
            this.tabMantenimiento = new System.Windows.Forms.TabControl();
            this.Menu = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listarClientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.equiposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listarEquiposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearEquipoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mantenimientosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.realizarMantenimientoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearMantenimientoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dtEquipo = new System.Windows.Forms.DataGridView();
            this.tabCrear.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabListado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtMantenimientos)).BeginInit();
            this.tabMantenimiento.SuspendLayout();
            this.Menu.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtEquipo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnEditarEquipo
            // 
            this.btnEditarEquipo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditarEquipo.Image = ((System.Drawing.Image)(resources.GetObject("btnEditarEquipo.Image")));
            this.btnEditarEquipo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEditarEquipo.Location = new System.Drawing.Point(1156, 274);
            this.btnEditarEquipo.Margin = new System.Windows.Forms.Padding(2);
            this.btnEditarEquipo.Name = "btnEditarEquipo";
            this.btnEditarEquipo.Size = new System.Drawing.Size(102, 75);
            this.btnEditarEquipo.TabIndex = 28;
            this.btnEditarEquipo.Text = "Editar";
            this.btnEditarEquipo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEditarEquipo.UseVisualStyleBackColor = true;
            this.btnEditarEquipo.Click += new System.EventHandler(this.btnEditarEquipo_Click);
            // 
            // btnCrearEquipo
            // 
            this.btnCrearEquipo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCrearEquipo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCrearEquipo.Image = ((System.Drawing.Image)(resources.GetObject("btnCrearEquipo.Image")));
            this.btnCrearEquipo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCrearEquipo.Location = new System.Drawing.Point(1156, 202);
            this.btnCrearEquipo.Margin = new System.Windows.Forms.Padding(2);
            this.btnCrearEquipo.Name = "btnCrearEquipo";
            this.btnCrearEquipo.Size = new System.Drawing.Size(102, 75);
            this.btnCrearEquipo.TabIndex = 25;
            this.btnCrearEquipo.Text = "Crear";
            this.btnCrearEquipo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCrearEquipo.UseVisualStyleBackColor = true;
            this.btnCrearEquipo.Click += new System.EventHandler(this.btnCrearEquipo_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLimpiar.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpiar.Image")));
            this.btnLimpiar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpiar.Location = new System.Drawing.Point(1156, 128);
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(2);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(102, 75);
            this.btnLimpiar.TabIndex = 26;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnVolver
            // 
            this.btnVolver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVolver.Image = ((System.Drawing.Image)(resources.GetObject("btnVolver.Image")));
            this.btnVolver.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVolver.Location = new System.Drawing.Point(1156, 347);
            this.btnVolver.Margin = new System.Windows.Forms.Padding(2);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(102, 75);
            this.btnVolver.TabIndex = 27;
            this.btnVolver.Text = "Volver";
            this.btnVolver.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // tabCrear
            // 
            this.tabCrear.Controls.Add(this.panel2);
            this.tabCrear.Controls.Add(this.txtHoras);
            this.tabCrear.Controls.Add(this.label17);
            this.tabCrear.Controls.Add(this.lbFecha);
            this.tabCrear.Controls.Add(this.label15);
            this.tabCrear.Controls.Add(this.txtEquipoId);
            this.tabCrear.Controls.Add(this.label14);
            this.tabCrear.Controls.Add(this.txtID);
            this.tabCrear.Controls.Add(this.txtInformacionAdicional);
            this.tabCrear.Controls.Add(this.label13);
            this.tabCrear.Controls.Add(this.label12);
            this.tabCrear.Controls.Add(this.label3);
            this.tabCrear.Controls.Add(this.panel1);
            this.tabCrear.Controls.Add(this.label2);
            this.tabCrear.Controls.Add(this.label1);
            this.tabCrear.Controls.Add(this.cbEquipo);
            this.tabCrear.Controls.Add(this.cbCliente);
            this.tabCrear.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabCrear.Location = new System.Drawing.Point(4, 29);
            this.tabCrear.Margin = new System.Windows.Forms.Padding(2);
            this.tabCrear.Name = "tabCrear";
            this.tabCrear.Padding = new System.Windows.Forms.Padding(2);
            this.tabCrear.Size = new System.Drawing.Size(1121, 504);
            this.tabCrear.TabIndex = 1;
            this.tabCrear.Text = "Crear / Editar";
            this.tabCrear.UseVisualStyleBackColor = true;
            // 
            // txtHoras
            // 
            this.txtHoras.Location = new System.Drawing.Point(742, 35);
            this.txtHoras.Margin = new System.Windows.Forms.Padding(2);
            this.txtHoras.Name = "txtHoras";
            this.txtHoras.Size = new System.Drawing.Size(76, 23);
            this.txtHoras.TabIndex = 49;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(578, 38);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(130, 18);
            this.label17.TabIndex = 48;
            this.label17.Text = "Horas Trabajadas:";
            // 
            // lbFecha
            // 
            this.lbFecha.AutoSize = true;
            this.lbFecha.Location = new System.Drawing.Point(845, 55);
            this.lbFecha.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbFecha.Name = "lbFecha";
            this.lbFecha.Size = new System.Drawing.Size(47, 17);
            this.lbFecha.TabIndex = 47;
            this.lbFecha.Text = "Fecha";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(859, 28);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 18);
            this.label15.TabIndex = 45;
            this.label15.Text = "ID Equipo:";
            // 
            // txtEquipoId
            // 
            this.txtEquipoId.Location = new System.Drawing.Point(983, 28);
            this.txtEquipoId.Margin = new System.Windows.Forms.Padding(2);
            this.txtEquipoId.Name = "txtEquipoId";
            this.txtEquipoId.ReadOnly = true;
            this.txtEquipoId.Size = new System.Drawing.Size(59, 23);
            this.txtEquipoId.TabIndex = 44;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(808, 2);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(127, 18);
            this.label14.TabIndex = 43;
            this.label14.Text = "ID Mantenimiento:";
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(983, 2);
            this.txtID.Margin = new System.Windows.Forms.Padding(2);
            this.txtID.Name = "txtID";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(59, 23);
            this.txtID.TabIndex = 42;
            // 
            // txtInformacionAdicional
            // 
            this.txtInformacionAdicional.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInformacionAdicional.Location = new System.Drawing.Point(37, 452);
            this.txtInformacionAdicional.Margin = new System.Windows.Forms.Padding(2);
            this.txtInformacionAdicional.Multiline = true;
            this.txtInformacionAdicional.Name = "txtInformacionAdicional";
            this.txtInformacionAdicional.Size = new System.Drawing.Size(1079, 54);
            this.txtInformacionAdicional.TabIndex = 38;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(-20, 0);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(22, 18);
            this.label13.TabIndex = 41;
            this.label13.Text = "ID";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(34, 432);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(149, 18);
            this.label12.TabIndex = 39;
            this.label12.Text = "Informacion Adicional";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(34, 174);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 18);
            this.label3.TabIndex = 37;
            this.label3.Text = "Cambios";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.cProximoMantenimiento);
            this.panel1.Controls.Add(this.groupBox8);
            this.panel1.Controls.Add(this.groupBox7);
            this.panel1.Controls.Add(this.groupBox6);
            this.panel1.Controls.Add(this.groupBox5);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Location = new System.Drawing.Point(37, 194);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1080, 236);
            this.panel1.TabIndex = 36;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(810, 4);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(154, 17);
            this.label16.TabIndex = 49;
            this.label16.Text = "Proximo Mantenimiento";
            // 
            // cProximoMantenimiento
            // 
            this.cProximoMantenimiento.Location = new System.Drawing.Point(810, 24);
            this.cProximoMantenimiento.Margin = new System.Windows.Forms.Padding(7);
            this.cProximoMantenimiento.Name = "cProximoMantenimiento";
            this.cProximoMantenimiento.TabIndex = 48;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.rbCorreaBombaAgua1B);
            this.groupBox8.Controls.Add(this.rbCorreaBombaAgua1A);
            this.groupBox8.Controls.Add(this.label11);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(357, 139);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox8.Size = new System.Drawing.Size(274, 32);
            this.groupBox8.TabIndex = 40;
            this.groupBox8.TabStop = false;
            // 
            // rbCorreaBombaAgua1B
            // 
            this.rbCorreaBombaAgua1B.AutoSize = true;
            this.rbCorreaBombaAgua1B.Location = new System.Drawing.Point(223, 7);
            this.rbCorreaBombaAgua1B.Margin = new System.Windows.Forms.Padding(2);
            this.rbCorreaBombaAgua1B.Name = "rbCorreaBombaAgua1B";
            this.rbCorreaBombaAgua1B.Size = new System.Drawing.Size(47, 21);
            this.rbCorreaBombaAgua1B.TabIndex = 29;
            this.rbCorreaBombaAgua1B.TabStop = true;
            this.rbCorreaBombaAgua1B.Text = "NO";
            this.rbCorreaBombaAgua1B.UseVisualStyleBackColor = true;
            // 
            // rbCorreaBombaAgua1A
            // 
            this.rbCorreaBombaAgua1A.AutoSize = true;
            this.rbCorreaBombaAgua1A.Location = new System.Drawing.Point(181, 7);
            this.rbCorreaBombaAgua1A.Margin = new System.Windows.Forms.Padding(2);
            this.rbCorreaBombaAgua1A.Name = "rbCorreaBombaAgua1A";
            this.rbCorreaBombaAgua1A.Size = new System.Drawing.Size(38, 21);
            this.rbCorreaBombaAgua1A.TabIndex = 28;
            this.rbCorreaBombaAgua1A.TabStop = true;
            this.rbCorreaBombaAgua1A.Text = "SI";
            this.rbCorreaBombaAgua1A.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 9);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(154, 17);
            this.label11.TabIndex = 30;
            this.label11.Text = "Correa bomba de agua";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.rbCorreaAlternador1B);
            this.groupBox7.Controls.Add(this.rbCorreaAlternador1A);
            this.groupBox7.Controls.Add(this.label10);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(383, 99);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox7.Size = new System.Drawing.Size(248, 32);
            this.groupBox7.TabIndex = 39;
            this.groupBox7.TabStop = false;
            // 
            // rbCorreaAlternador1B
            // 
            this.rbCorreaAlternador1B.AutoSize = true;
            this.rbCorreaAlternador1B.Location = new System.Drawing.Point(197, 7);
            this.rbCorreaAlternador1B.Margin = new System.Windows.Forms.Padding(2);
            this.rbCorreaAlternador1B.Name = "rbCorreaAlternador1B";
            this.rbCorreaAlternador1B.Size = new System.Drawing.Size(47, 21);
            this.rbCorreaAlternador1B.TabIndex = 29;
            this.rbCorreaAlternador1B.TabStop = true;
            this.rbCorreaAlternador1B.Text = "NO";
            this.rbCorreaAlternador1B.UseVisualStyleBackColor = true;
            // 
            // rbCorreaAlternador1A
            // 
            this.rbCorreaAlternador1A.AutoSize = true;
            this.rbCorreaAlternador1A.Location = new System.Drawing.Point(155, 7);
            this.rbCorreaAlternador1A.Margin = new System.Windows.Forms.Padding(2);
            this.rbCorreaAlternador1A.Name = "rbCorreaAlternador1A";
            this.rbCorreaAlternador1A.Size = new System.Drawing.Size(38, 21);
            this.rbCorreaAlternador1A.TabIndex = 28;
            this.rbCorreaAlternador1A.TabStop = true;
            this.rbCorreaAlternador1A.Text = "SI";
            this.rbCorreaAlternador1A.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 9);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 17);
            this.label10.TabIndex = 29;
            this.label10.Text = "Correa alternador";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.rbAceite1B);
            this.groupBox6.Controls.Add(this.rbAceite1A);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(386, 16);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox6.Size = new System.Drawing.Size(245, 32);
            this.groupBox6.TabIndex = 38;
            this.groupBox6.TabStop = false;
            // 
            // rbAceite1B
            // 
            this.rbAceite1B.AutoSize = true;
            this.rbAceite1B.Location = new System.Drawing.Point(194, 6);
            this.rbAceite1B.Margin = new System.Windows.Forms.Padding(2);
            this.rbAceite1B.Name = "rbAceite1B";
            this.rbAceite1B.Size = new System.Drawing.Size(47, 21);
            this.rbAceite1B.TabIndex = 29;
            this.rbAceite1B.TabStop = true;
            this.rbAceite1B.Text = "NO";
            this.rbAceite1B.UseVisualStyleBackColor = true;
            // 
            // rbAceite1A
            // 
            this.rbAceite1A.AutoSize = true;
            this.rbAceite1A.Location = new System.Drawing.Point(152, 6);
            this.rbAceite1A.Margin = new System.Windows.Forms.Padding(2);
            this.rbAceite1A.Name = "rbAceite1A";
            this.rbAceite1A.Size = new System.Drawing.Size(38, 21);
            this.rbAceite1A.TabIndex = 28;
            this.rbAceite1A.TabStop = true;
            this.rbAceite1A.Text = "SI";
            this.rbAceite1A.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(82, 9);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 17);
            this.label7.TabIndex = 27;
            this.label7.Text = "Aceite";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rbCooland1B);
            this.groupBox5.Controls.Add(this.rbCooland1A);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(386, 54);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox5.Size = new System.Drawing.Size(245, 32);
            this.groupBox5.TabIndex = 37;
            this.groupBox5.TabStop = false;
            // 
            // rbCooland1B
            // 
            this.rbCooland1B.AutoSize = true;
            this.rbCooland1B.Location = new System.Drawing.Point(194, 7);
            this.rbCooland1B.Margin = new System.Windows.Forms.Padding(2);
            this.rbCooland1B.Name = "rbCooland1B";
            this.rbCooland1B.Size = new System.Drawing.Size(47, 21);
            this.rbCooland1B.TabIndex = 29;
            this.rbCooland1B.TabStop = true;
            this.rbCooland1B.Text = "NO";
            this.rbCooland1B.UseVisualStyleBackColor = true;
            // 
            // rbCooland1A
            // 
            this.rbCooland1A.AutoSize = true;
            this.rbCooland1A.Location = new System.Drawing.Point(152, 7);
            this.rbCooland1A.Margin = new System.Windows.Forms.Padding(2);
            this.rbCooland1A.Name = "rbCooland1A";
            this.rbCooland1A.Size = new System.Drawing.Size(38, 21);
            this.rbCooland1A.TabIndex = 28;
            this.rbCooland1A.TabStop = true;
            this.rbCooland1A.Text = "SI";
            this.rbCooland1A.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(72, 9);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 17);
            this.label8.TabIndex = 26;
            this.label8.Text = "Cooland";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.rbFiltroAgua1B);
            this.groupBox4.Controls.Add(this.rbFiltroAgua1A);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(28, 139);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(303, 32);
            this.groupBox4.TabIndex = 36;
            this.groupBox4.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 9);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(163, 17);
            this.label6.TabIndex = 30;
            this.label6.Text = "Filtro de trampa de agua";
            // 
            // rbFiltroAgua1B
            // 
            this.rbFiltroAgua1B.AutoSize = true;
            this.rbFiltroAgua1B.Location = new System.Drawing.Point(252, 7);
            this.rbFiltroAgua1B.Margin = new System.Windows.Forms.Padding(2);
            this.rbFiltroAgua1B.Name = "rbFiltroAgua1B";
            this.rbFiltroAgua1B.Size = new System.Drawing.Size(47, 21);
            this.rbFiltroAgua1B.TabIndex = 29;
            this.rbFiltroAgua1B.TabStop = true;
            this.rbFiltroAgua1B.Text = "NO";
            this.rbFiltroAgua1B.UseVisualStyleBackColor = true;
            // 
            // rbFiltroAgua1A
            // 
            this.rbFiltroAgua1A.AutoSize = true;
            this.rbFiltroAgua1A.Location = new System.Drawing.Point(210, 7);
            this.rbFiltroAgua1A.Margin = new System.Windows.Forms.Padding(2);
            this.rbFiltroAgua1A.Name = "rbFiltroAgua1A";
            this.rbFiltroAgua1A.Size = new System.Drawing.Size(38, 21);
            this.rbFiltroAgua1A.TabIndex = 28;
            this.rbFiltroAgua1A.TabStop = true;
            this.rbFiltroAgua1A.Text = "SI";
            this.rbFiltroAgua1A.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbFiltroAire1B);
            this.groupBox3.Controls.Add(this.rbFiltroAire1A);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(69, 99);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(262, 32);
            this.groupBox3.TabIndex = 35;
            this.groupBox3.TabStop = false;
            // 
            // rbFiltroAire1B
            // 
            this.rbFiltroAire1B.AutoSize = true;
            this.rbFiltroAire1B.Location = new System.Drawing.Point(211, 7);
            this.rbFiltroAire1B.Margin = new System.Windows.Forms.Padding(2);
            this.rbFiltroAire1B.Name = "rbFiltroAire1B";
            this.rbFiltroAire1B.Size = new System.Drawing.Size(47, 21);
            this.rbFiltroAire1B.TabIndex = 29;
            this.rbFiltroAire1B.TabStop = true;
            this.rbFiltroAire1B.Text = "NO";
            this.rbFiltroAire1B.UseVisualStyleBackColor = true;
            // 
            // rbFiltroAire1A
            // 
            this.rbFiltroAire1A.AutoSize = true;
            this.rbFiltroAire1A.Location = new System.Drawing.Point(169, 7);
            this.rbFiltroAire1A.Margin = new System.Windows.Forms.Padding(2);
            this.rbFiltroAire1A.Name = "rbFiltroAire1A";
            this.rbFiltroAire1A.Size = new System.Drawing.Size(38, 21);
            this.rbFiltroAire1A.TabIndex = 28;
            this.rbFiltroAire1A.TabStop = true;
            this.rbFiltroAire1A.Text = "SI";
            this.rbFiltroAire1A.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(46, 9);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 17);
            this.label9.TabIndex = 25;
            this.label9.Text = "Filtro de aire";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbFiltroGasoil1B);
            this.groupBox2.Controls.Add(this.rbFiltroGasoil1A);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(69, 53);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(262, 32);
            this.groupBox2.TabIndex = 34;
            this.groupBox2.TabStop = false;
            // 
            // rbFiltroGasoil1B
            // 
            this.rbFiltroGasoil1B.AutoSize = true;
            this.rbFiltroGasoil1B.Location = new System.Drawing.Point(211, 7);
            this.rbFiltroGasoil1B.Margin = new System.Windows.Forms.Padding(2);
            this.rbFiltroGasoil1B.Name = "rbFiltroGasoil1B";
            this.rbFiltroGasoil1B.Size = new System.Drawing.Size(47, 21);
            this.rbFiltroGasoil1B.TabIndex = 29;
            this.rbFiltroGasoil1B.TabStop = true;
            this.rbFiltroGasoil1B.Text = "NO";
            this.rbFiltroGasoil1B.UseVisualStyleBackColor = true;
            // 
            // rbFiltroGasoil1A
            // 
            this.rbFiltroGasoil1A.AutoSize = true;
            this.rbFiltroGasoil1A.Location = new System.Drawing.Point(169, 7);
            this.rbFiltroGasoil1A.Margin = new System.Windows.Forms.Padding(2);
            this.rbFiltroGasoil1A.Name = "rbFiltroGasoil1A";
            this.rbFiltroGasoil1A.Size = new System.Drawing.Size(38, 21);
            this.rbFiltroGasoil1A.TabIndex = 28;
            this.rbFiltroGasoil1A.TabStop = true;
            this.rbFiltroGasoil1A.Text = "SI";
            this.rbFiltroGasoil1A.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 9);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 17);
            this.label5.TabIndex = 23;
            this.label5.Text = "Filtro de gasoil";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbFiltroAceite1B);
            this.groupBox1.Controls.Add(this.rbFiltroAceite1A);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(69, 13);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(262, 32);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            // 
            // rbFiltroAceite1B
            // 
            this.rbFiltroAceite1B.AutoSize = true;
            this.rbFiltroAceite1B.Location = new System.Drawing.Point(211, 7);
            this.rbFiltroAceite1B.Margin = new System.Windows.Forms.Padding(2);
            this.rbFiltroAceite1B.Name = "rbFiltroAceite1B";
            this.rbFiltroAceite1B.Size = new System.Drawing.Size(47, 21);
            this.rbFiltroAceite1B.TabIndex = 29;
            this.rbFiltroAceite1B.TabStop = true;
            this.rbFiltroAceite1B.Text = "NO";
            this.rbFiltroAceite1B.UseVisualStyleBackColor = true;
            // 
            // rbFiltroAceite1A
            // 
            this.rbFiltroAceite1A.AutoSize = true;
            this.rbFiltroAceite1A.Location = new System.Drawing.Point(169, 8);
            this.rbFiltroAceite1A.Margin = new System.Windows.Forms.Padding(2);
            this.rbFiltroAceite1A.Name = "rbFiltroAceite1A";
            this.rbFiltroAceite1A.Size = new System.Drawing.Size(38, 21);
            this.rbFiltroAceite1A.TabIndex = 28;
            this.rbFiltroAceite1A.TabStop = true;
            this.rbFiltroAceite1A.Text = "SI";
            this.rbFiltroAceite1A.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 9);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 17);
            this.label4.TabIndex = 22;
            this.label4.Text = "Filtro de aceite";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(307, 36);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 18);
            this.label2.TabIndex = 35;
            this.label2.Text = "Equipo:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 18);
            this.label1.TabIndex = 34;
            this.label1.Text = "Cliente:";
            // 
            // cbEquipo
            // 
            this.cbEquipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEquipo.FormattingEnabled = true;
            this.cbEquipo.Location = new System.Drawing.Point(384, 36);
            this.cbEquipo.Margin = new System.Windows.Forms.Padding(2);
            this.cbEquipo.Name = "cbEquipo";
            this.cbEquipo.Size = new System.Drawing.Size(190, 24);
            this.cbEquipo.TabIndex = 33;
            // 
            // cbCliente
            // 
            this.cbCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCliente.FormattingEnabled = true;
            this.cbCliente.Location = new System.Drawing.Point(107, 37);
            this.cbCliente.Margin = new System.Windows.Forms.Padding(2);
            this.cbCliente.Name = "cbCliente";
            this.cbCliente.Size = new System.Drawing.Size(190, 24);
            this.cbCliente.TabIndex = 32;
            this.cbCliente.SelectedIndexChanged += new System.EventHandler(this.cbCliente_SelectedIndexChanged_1);
            // 
            // tabListado
            // 
            this.tabListado.Controls.Add(this.dtMantenimientos);
            this.tabListado.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabListado.Location = new System.Drawing.Point(4, 29);
            this.tabListado.Margin = new System.Windows.Forms.Padding(2);
            this.tabListado.Name = "tabListado";
            this.tabListado.Padding = new System.Windows.Forms.Padding(2);
            this.tabListado.Size = new System.Drawing.Size(1121, 504);
            this.tabListado.TabIndex = 0;
            this.tabListado.Text = "Listado";
            this.tabListado.UseVisualStyleBackColor = true;
            // 
            // dtMantenimientos
            // 
            this.dtMantenimientos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtMantenimientos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtMantenimientos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtMantenimientos.Location = new System.Drawing.Point(4, 33);
            this.dtMantenimientos.Margin = new System.Windows.Forms.Padding(2);
            this.dtMantenimientos.Name = "dtMantenimientos";
            this.dtMantenimientos.RowTemplate.Height = 24;
            this.dtMantenimientos.Size = new System.Drawing.Size(1040, 240);
            this.dtMantenimientos.TabIndex = 0;
            this.dtMantenimientos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtMantenimientos_CellClick);
            // 
            // tabMantenimiento
            // 
            this.tabMantenimiento.Controls.Add(this.tabListado);
            this.tabMantenimiento.Controls.Add(this.tabCrear);
            this.tabMantenimiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabMantenimiento.Location = new System.Drawing.Point(9, 38);
            this.tabMantenimiento.Margin = new System.Windows.Forms.Padding(2);
            this.tabMantenimiento.Name = "tabMantenimiento";
            this.tabMantenimiento.SelectedIndex = 0;
            this.tabMantenimiento.Size = new System.Drawing.Size(1129, 537);
            this.tabMantenimiento.TabIndex = 29;
            // 
            // Menu
            // 
            this.Menu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.clientesToolStripMenuItem,
            this.equiposToolStripMenuItem,
            this.mantenimientosToolStripMenuItem});
            this.Menu.Location = new System.Drawing.Point(0, 0);
            this.Menu.Name = "Menu";
            this.Menu.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.Menu.Size = new System.Drawing.Size(1267, 28);
            this.Menu.TabIndex = 30;
            this.Menu.Text = "menuStrip2";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 24);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(96, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            // 
            // clientesToolStripMenuItem
            // 
            this.clientesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listarClientesToolStripMenuItem,
            this.crearClienteToolStripMenuItem});
            this.clientesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("clientesToolStripMenuItem.Image")));
            this.clientesToolStripMenuItem.Name = "clientesToolStripMenuItem";
            this.clientesToolStripMenuItem.Size = new System.Drawing.Size(81, 24);
            this.clientesToolStripMenuItem.Text = "Clientes";
            // 
            // listarClientesToolStripMenuItem
            // 
            this.listarClientesToolStripMenuItem.Name = "listarClientesToolStripMenuItem";
            this.listarClientesToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.listarClientesToolStripMenuItem.Text = "Listar Clientes";
            // 
            // crearClienteToolStripMenuItem
            // 
            this.crearClienteToolStripMenuItem.Name = "crearClienteToolStripMenuItem";
            this.crearClienteToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.crearClienteToolStripMenuItem.Text = "Crear Cliente";
            // 
            // equiposToolStripMenuItem
            // 
            this.equiposToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listarEquiposToolStripMenuItem,
            this.crearEquipoToolStripMenuItem});
            this.equiposToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("equiposToolStripMenuItem.Image")));
            this.equiposToolStripMenuItem.Name = "equiposToolStripMenuItem";
            this.equiposToolStripMenuItem.Size = new System.Drawing.Size(81, 24);
            this.equiposToolStripMenuItem.Text = "Equipos";
            // 
            // listarEquiposToolStripMenuItem
            // 
            this.listarEquiposToolStripMenuItem.Name = "listarEquiposToolStripMenuItem";
            this.listarEquiposToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.listarEquiposToolStripMenuItem.Text = "Listar Equipos";
            // 
            // crearEquipoToolStripMenuItem
            // 
            this.crearEquipoToolStripMenuItem.Name = "crearEquipoToolStripMenuItem";
            this.crearEquipoToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.crearEquipoToolStripMenuItem.Text = "Crear Equipo";
            // 
            // mantenimientosToolStripMenuItem
            // 
            this.mantenimientosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.realizarMantenimientoToolStripMenuItem,
            this.crearMantenimientoToolStripMenuItem});
            this.mantenimientosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("mantenimientosToolStripMenuItem.Image")));
            this.mantenimientosToolStripMenuItem.Name = "mantenimientosToolStripMenuItem";
            this.mantenimientosToolStripMenuItem.Size = new System.Drawing.Size(126, 24);
            this.mantenimientosToolStripMenuItem.Text = "Mantenimientos";
            // 
            // realizarMantenimientoToolStripMenuItem
            // 
            this.realizarMantenimientoToolStripMenuItem.Name = "realizarMantenimientoToolStripMenuItem";
            this.realizarMantenimientoToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.realizarMantenimientoToolStripMenuItem.Text = "Listar Mantenimientos";
            // 
            // crearMantenimientoToolStripMenuItem
            // 
            this.crearMantenimientoToolStripMenuItem.Name = "crearMantenimientoToolStripMenuItem";
            this.crearMantenimientoToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.crearMantenimientoToolStripMenuItem.Text = "Crear Mantenimiento";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dtEquipo);
            this.panel2.Location = new System.Drawing.Point(37, 71);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1079, 100);
            this.panel2.TabIndex = 50;
            // 
            // dtEquipo
            // 
            this.dtEquipo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dtEquipo.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtEquipo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtEquipo.Location = new System.Drawing.Point(5, 8);
            this.dtEquipo.Margin = new System.Windows.Forms.Padding(2);
            this.dtEquipo.Name = "dtEquipo";
            this.dtEquipo.RowTemplate.Height = 24;
            this.dtEquipo.Size = new System.Drawing.Size(1069, 84);
            this.dtEquipo.TabIndex = 41;
            // 
            // frmMantenimiento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1267, 577);
            this.Controls.Add(this.Menu);
            this.Controls.Add(this.tabMantenimiento);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btnCrearEquipo);
            this.Controls.Add(this.btnEditarEquipo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmMantenimiento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mantenimiento";
            this.Load += new System.EventHandler(this.frmMantenimiento_Load);
            this.tabCrear.ResumeLayout(false);
            this.tabCrear.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabListado.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtMantenimientos)).EndInit();
            this.tabMantenimiento.ResumeLayout(false);
            this.Menu.ResumeLayout(false);
            this.Menu.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtEquipo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnEditarEquipo;
        private System.Windows.Forms.Button btnCrearEquipo;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnVolver;
        public System.Windows.Forms.TabPage tabCrear;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.TextBox txtInformacionAdicional;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton rbCorreaBombaAgua1B;
        private System.Windows.Forms.RadioButton rbCorreaBombaAgua1A;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton rbCorreaAlternador1B;
        private System.Windows.Forms.RadioButton rbCorreaAlternador1A;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton rbAceite1B;
        private System.Windows.Forms.RadioButton rbAceite1A;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton rbCooland1B;
        private System.Windows.Forms.RadioButton rbCooland1A;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton rbFiltroAgua1B;
        private System.Windows.Forms.RadioButton rbFiltroAgua1A;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rbFiltroAire1B;
        private System.Windows.Forms.RadioButton rbFiltroAire1A;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbFiltroGasoil1B;
        private System.Windows.Forms.RadioButton rbFiltroGasoil1A;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbFiltroAceite1B;
        private System.Windows.Forms.RadioButton rbFiltroAceite1A;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbEquipo;
        private System.Windows.Forms.ComboBox cbCliente;
        public System.Windows.Forms.TabPage tabListado;
        public System.Windows.Forms.DataGridView dtMantenimientos;
        public System.Windows.Forms.TabControl tabMantenimiento;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtEquipoId;
        private System.Windows.Forms.MonthCalendar cProximoMantenimiento;
        private System.Windows.Forms.Label lbFecha;
        private System.Windows.Forms.TextBox txtHoras;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.MenuStrip Menu;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listarClientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem equiposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listarEquiposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearEquipoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mantenimientosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem realizarMantenimientoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearMantenimientoToolStripMenuItem;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dtEquipo;
    }
}