﻿using ManApp.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;

using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManApp.UI
{

    public partial class frmEquipo : Form
    {
        OleDbConnection con = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\MrMartinez\Documents\Developer\ManApp\BD.accdb");
        SQLDataAccessHelper helper = new SQLDataAccessHelper();
        public frmEquipo()
        {
            InitializeComponent();
        }
        private void frmEquipo_Load(object sender, EventArgs e)
        {
            llenarGrid();
            llenarCbCliente();
            estadoBotones();

        }
        private void estadoBotones()
        {
            btnCrearEquipo.Enabled = true;
            btnLimpiar.Enabled = false;
            btnEditarEquipo.Enabled = false;
            if (txtID.Text != "")
            {
                btnCrearEquipo.Enabled = false;
            }
            else
            {
                btnEditarEquipo.Enabled = false;
            }
        }

        private void btnCrearEquipo_Click(object sender, EventArgs e)
        {
            CrearEquipo();
        }

        private void CrearEquipo()
        {
            try
            {
                if (txtDescripcion.Text != "")
                {
                    //Manera anterior de crear un equipo  -Se cambia para el SQLHelper
                    #region 
                    //con.Open();
                    //OleDbCommand cmd = con.CreateCommand();
                    //cmd.CommandType = CommandType.Text;
                    //cmd.CommandText = "INSERT INTO Equipos(EmpresaId, Descripcion, Serial, Marca, Motor, RPM,HP, Generador, KW, Voltios, Amperes, HZ, Conexion) " +
                    //                  "              VALUES(@ClienteId, @Descripcion, @Serial, @Marca, @Motor, @RPM, @HP, @Generador, @KW, @Voltios, @Amperes, @HZ, @Conexion)";

                    //cmd.Parameters.AddWithValue("@EmpresaId", cbClientes.SelectedValue);
                    //cmd.Parameters.AddWithValue("@Nombre", txtDescripcion.Text);
                    //cmd.Parameters.AddWithValue("@Serial", txtSerial.Text);
                    //cmd.Parameters.AddWithValue("@Marca", txtMarca.Text);
                    //cmd.Parameters.AddWithValue("@Motor", txtMotor.Text);
                    //cmd.Parameters.AddWithValue("@RPM", txtRPM.Text);
                    //cmd.Parameters.AddWithValue("@HP", txtHP.Text);
                    //cmd.Parameters.AddWithValue("@Generador", txtGenerador.Text);
                    //cmd.Parameters.AddWithValue("@KW", txtKW.Text);
                    //cmd.Parameters.AddWithValue("@Voltios", txtVoltios.Text);
                    //cmd.Parameters.AddWithValue("@Amperes", txtAmperes.Text);
                    //cmd.Parameters.AddWithValue("@HZ", txtHZ.Text);
                    //cmd.Parameters.AddWithValue("@Conexion", txtConexion.Text);

                    //cmd.ExecuteNonQuery();
                    #endregion

                    OleDbParameter[] parameters = {

                             new OleDbParameter("@EmpresaId", cbClientes.SelectedValue),
                             new OleDbParameter("@Descripcion", txtDescripcion.Text),
                             new OleDbParameter("@Serial", txtSerial.Text),
                             new OleDbParameter("@Marca", txtMarca.Text),
                             new OleDbParameter("@Motor",txtMotor.Text),
                             new OleDbParameter("@RPM", txtRPM.Text),
                             new OleDbParameter("@HP", txtHP.Text),
                             new OleDbParameter("@Generador", txtGenerador.Text),
                             new OleDbParameter("@KW", txtKW.Text),
                             new OleDbParameter("@Voltios", txtVoltios.Text),
                             new OleDbParameter("@Amperes", txtAmperes.Text),
                             new OleDbParameter("@Hz", txtHZ.Text),
                             new OleDbParameter("@Conexion", txtConexion.Text),
                    };
                    var resultado = helper.ExecuteNonQuery("INSERT INTO Equipos(EmpresaId, Descripcion, Serial, Marca, Motor, RPM, HP, Generador, KW, Voltios, Amperes, Hz, Conexion) " +
                                                             "VALUES(@EmpresaId, @Descripcion, @Serial, @Marca, @Motor, @RPM, @HP, @Generador, @KW, @Voltios, @Amperes, @Hz, @Conexion)",
                                                             CommandType.Text, parameters);
            
                    MessageBox.Show("Guardo con exito");
                    limpiarCampos();
                    llenarGrid();

                    
                }
                else
                {
                    MessageBox.Show("Falta el nombre de la empresa");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error guardando los datos" + ex);
            }
        }

        private void EditarEquipo()
        {

            try
            {
                if (txtDescripcion.Text != "")
                {
                   
                    con.Open();
                    OleDbCommand cmd = con.CreateCommand();
                    cmd.CommandType = CommandType.Text;
           
                    cmd.CommandText = @"UPDATE Equipos SET  EmpresaId = "+cbClientes.SelectedValue+", Descripcion = ' " + txtDescripcion.Text + " ', " +
                                                            "Serial = ' " + txtSerial.Text + "'," +
                                                            "Marca = '" + txtMarca.Text + "', " +
                                                            "Motor = '" + txtMotor.Text + "', " +
                                                            "RPM = '" + txtRPM.Text + "'," +
                                                            "HP = '" + txtHP.Text + "', " +
                                                            "Generador = '" + txtGenerador.Text + "', " +
                                                            "KW = '" + txtKW.Text + "', " +
                                                            "Voltios = '" + txtVoltios.Text + "', " +
                                                            "Amperes ='" + txtAmperes.Text + "', " +
                                                            "Hz = '" + txtHZ.Text + "', " +
                                                            "Conexion = '" + txtConexion.Text + "' " +
                                                            "WHERE Id = "+txtID.Text+"";
                    cmd.ExecuteNonQuery();
                    con.Close();


                    MessageBox.Show("Registro Modificado");
                    limpiarCampos();
                    llenarGrid();
                    tbEquipos.SelectedTab = tabListado;
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Error modificando los datos" + ex);
            }
        }

        private void limpiarCampos()
        {
            txtID.Text = "";
            cbClientes.Text = "";
            txtDescripcion.Text = "";
            txtSerial.Text = "";
            txtMarca.Text = "";
            txtMotor.Text = "";
            txtRPM.Text = "";
            txtHP.Text = "";
            txtGenerador.Text = "";
            txtKW.Text = "";
            txtVoltios.Text = "";
            txtAmperes.Text = "";
            txtHZ.Text = "";
            txtConexion.Text = "";
            estadoBotones();
        }

        private void llenarGrid()
        {
            // Codigo anterior para llenar el grid -Se cambio para llenar con el SQLHelper
            #region 
            //con.Open();
            //OleDbCommand cmd = con.CreateCommand();
            //cmd.CommandType = CommandType.Text;
            //cmd.CommandText = "select * from Equipos";
            //cmd.ExecuteNonQuery();

            ////Crear la tabla
            //DataTable dt = new DataTable();
            //OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            //da.Fill(dt);
            //dtEquipos.DataSource = null;
            //dtEquipos.DataSource = dt;
            //con.Close();
            #endregion
            var lista = helper.ExecuteQuery("select Id, Descripcion, Marca, Serial, Nombre from vEquiposEmpresas", CommandType.Text, null).Tables[0];

            dtEquipos.DataSource = null;
            dtEquipos.DataSource = lista;
            tbEquipos.SelectedTab = tabListado;

        }

        private void LlenarCampos(int id)
        {
            llenarCbCliente();
            DataTable consulta = helper.ExecuteQuery("SELECT * FROM Equipos WHERE Id = " + id + " ", CommandType.Text, null).Tables[0];
            txtID.Text = consulta.Rows[0][0].ToString();
            cbClientes.SelectedValue = consulta.Rows[0][1].ToString();
            txtDescripcion.Text = consulta.Rows[0][2].ToString();
            txtSerial.Text = consulta.Rows[0][3].ToString();
            txtMarca.Text = consulta.Rows[0][4].ToString();
            txtMotor.Text = consulta.Rows[0][5].ToString();
            txtRPM.Text = consulta.Rows[0][6].ToString();
            txtHP.Text = consulta.Rows[0][7].ToString();
            txtGenerador.Text = consulta.Rows[0][8].ToString();
            txtKW.Text = consulta.Rows[0][9].ToString();
            txtVoltios.Text = consulta.Rows[0][10].ToString();
            txtAmperes.Text = consulta.Rows[0][11].ToString();
            txtHZ.Text = consulta.Rows[0][12].ToString();
            txtConexion.Text = consulta.Rows[0][13].ToString();

        }

        private void llenarCbCliente()
        {
            con.Open();
            OleDbCommand cmd = new OleDbCommand("SELECT EmpresaId, Nombre FROM Empresas");
            cmd.Connection = con;
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            DataRow row = dt.NewRow();
            row["Nombre"] = "Propietario del equipo";
            dt.Rows.InsertAt(row, 0);
            cbClientes.ValueMember = "EmpresaId";
            cbClientes.DisplayMember = "Nombre";
            cbClientes.DataSource = dt;
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            frmPrincipal frmPrincipal = new frmPrincipal();
            this.Close();
            frmPrincipal.Show();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            limpiarCampos();
        }

        private void dtEquipos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id = (int)dtEquipos.Rows[e.RowIndex].Cells[0].Value;
            LlenarCampos(id);

            btnEditarEquipo.Enabled = true;
            btnLimpiar.Enabled = true;
            btnCrearEquipo.Enabled = false;
            //tbClientes.SelectedIndex = 1;
            tbEquipos.SelectedTab = tabCrear;
            txtDescripcion.Focus();
        }

        private void btnEditarEquipo_Click(object sender, EventArgs e)
        {
            EditarEquipo();
        }

      
    }
}
