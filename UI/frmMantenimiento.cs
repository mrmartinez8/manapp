﻿using ManApp.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManApp.UI
{
    public partial class frmMantenimiento : Form
    {
        SQLDataAccessHelper helper = new SQLDataAccessHelper();
        OleDbConnection con = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\MrMartinez\Documents\Developer\ManApp\BD.accdb");

        public frmMantenimiento()
        {
            InitializeComponent();
        }

        private void frmMantenimiento_Load(object sender, EventArgs e)
        {
            llenarComboCliente();
            llenarGridPrincipal();
            lbFecha.Text = DateTime.Now.ToString();

            //estadoBotones();
           
        }
        public void estadoBotones()
        {
            btnCrearEquipo.Enabled = true;
            btnLimpiar.Enabled = false;
            btnEditarEquipo.Enabled = false;
            if (txtID.Text != "")
            {
                btnCrearEquipo.Enabled = false;
            }
            else
            {
                btnEditarEquipo.Enabled = false;
            }
        }
        private void llenarComboCliente()
        {
            con.Open();
            OleDbCommand cmd = new OleDbCommand("SELECT EmpresaId, Nombre FROM Empresas");
            cmd.Connection = con;
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            DataRow row = dt.NewRow();
            row["Nombre"] = "Propietario del equipo";
            dt.Rows.InsertAt(row, 0);
            cbCliente.ValueMember = "EmpresaId";
            cbCliente.DisplayMember = "Nombre";
            cbCliente.DataSource = dt;
        }

        private void llenarComboEquipo(string id)
        {
            con.Open();
            OleDbCommand cmd = new OleDbCommand("SELECT Id, Descripcion FROM Equipos WHERE EmpresaId=@id", con);
            cmd.Parameters.AddWithValue("id", id);
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            DataRow row = dt.NewRow();
            row["Descripcion"] = "Seleccione un equipo";
            dt.Rows.InsertAt(row, 0);
            cbEquipo.ValueMember = "Id";
            cbEquipo.DisplayMember = "Descripcion";
            cbEquipo.DataSource = dt;
        }


        private void btnVolver_Click(object sender, EventArgs e)
        {
            frmPrincipal frmPrincipal = new frmPrincipal();
            frmPrincipal.Show();
            this.Close();

        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            limpiarCampos();
        }

        private void btnCrearEquipo_Click(object sender, EventArgs e)
        {
            guardarMantenimiento();
        }

        private void guardarMantenimiento()
        {
            try
            {
                DateTime fProximoMantimiento = DateTime.Now.AddDays(15);
                if (cProximoMantenimiento.SelectionStart == cProximoMantenimiento.TodayDate)
                {
                    cProximoMantenimiento.SelectionStart = cProximoMantenimiento.SelectionStart.AddDays(15);
                }
                OleDbParameter[] parameters = {
                            new OleDbParameter("@Equipo", cbEquipo.SelectedValue),
                            new OleDbParameter("@Fecha", lbFecha.Text),
                            new OleDbParameter("@FiltroAceite", rbFiltroAceite1A.Checked),
                            new OleDbParameter("@FiltroGasoil", rbFiltroGasoil1A.Checked),
                            new OleDbParameter("@FiltroAire", rbFiltroAire1A.Checked),
                            new OleDbParameter("@FiltroAgua", rbFiltroAgua1A.Checked),
                            new OleDbParameter("@Cooland", rbCooland1A.Checked),
                            new OleDbParameter("@Aceite",rbFiltroAceite1A.Checked),
                            new OleDbParameter("@CorreaAlternador", rbCorreaAlternador1A.Checked),
                            new OleDbParameter("@CorreaBombaAgua",rbCorreaBombaAgua1A.Checked),
                            new OleDbParameter("@InfoAdicional",txtInformacionAdicional.Text),
                            new OleDbParameter("@ProximoMantenimiento",cProximoMantenimiento.SelectionStart),
                            new OleDbParameter("@HorasTrabajadas",txtHoras.Text),

                    };
              
                var r = helper.ExecuteNonQuery("INSERT INTO Mantenimientos(EquipoId, Fecha, FiltroAceite, FiltroGasoil, FiltroAire, FiltroAgua, Cooland, Aceite, CorreaAlternador, CorreaBombaAgua, InfoAdicional, ProximoMantenimiento, HorasTrabajadas) " +
                    "                                                     VALUES(@Equipo,@Fecha ,@FiltroAceite, @FiltroGasoil, @FiltroAire, @FiltroAgua, @Cooland, @Aceite, @CorreaAlternador, @CorreaBombaAgua, @InfoAdicional, @ProximoMantenimiento, @HorasTrabajadas)", CommandType.Text, parameters);

                MessageBox.Show("Guardo con exito");
                limpiarCampos();
                llenarGridPrincipal();
            }
            catch (Exception)
            {

                MessageBox.Show("Error creando equipo. Contacte a soporte tecnico!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void limpiarCampos()
        {
            txtID.Text = "";
            txtEquipoId.Text = "";
            cbCliente.SelectedIndex = 0;
            cbEquipo.SelectedIndex = 0;
            rbFiltroAceite1A.Checked = false;
            rbFiltroAceite1B.Checked = false;
            rbFiltroGasoil1A.Checked = false;
            rbFiltroGasoil1B.Checked = false;
            rbFiltroAire1A.Checked = false;
            rbFiltroAire1B.Checked = false;
            rbFiltroAgua1A.Checked = false;
            rbFiltroAgua1B.Checked = false;
            rbAceite1A.Checked = false;
            rbCooland1A.Checked = false;
            rbCorreaAlternador1A.Checked = false;
            rbCorreaBombaAgua1A.Checked = false;
            txtInformacionAdicional.Text = "";
            txtHoras.Text = "";
            cProximoMantenimiento.SelectionStart = DateTime.Now;
            estadoBotones();
        }

        private void btnEditarEquipo_Click(object sender, EventArgs e)
        {
            editarMantenimiento();
        }

        private void editarMantenimiento()
        {
            try
            {
                if (txtID.Text != "")
                {
                    btnCrearEquipo.Enabled = false;               

                    con.Open();
                    OleDbCommand cmd = con.CreateCommand();
                    //OleDbParameter[] parameters = {
                    //        new OleDbParameter("@ID", Convert.ToInt32(txtID.Text)),
                    //        new OleDbParameter("@EquipoId", cbEquipo.SelectedValue),
                    //        new OleDbParameter("@Fecha", lbFecha.Text),
                    //        new OleDbParameter("@FiltroAceite", rbFiltroAceite1A.Checked),
                    //        new OleDbParameter("@FiltroGasoil", rbFiltroGasoil1A.Checked),
                    //        new OleDbParameter("@FiltroAire", rbFiltroAire1A.Checked),
                    //        new OleDbParameter("@FiltroAgua", rbFiltroAgua1A.Checked),
                    //        new OleDbParameter("@Cooland", rbCooland1A.Checked),
                    //        new OleDbParameter("@Aceite",rbFiltroAceite1A.Checked),
                    //        new OleDbParameter("@CorreaAlternador", rbCorreaAlternador1A.Checked),
                    //        new OleDbParameter("@CorreaBombaAgua",rbCorreaBombaAgua1A.Checked),
                    //        new OleDbParameter("@InfoAdicional",txtInformacionAdicional.Text),
                    //        new OleDbParameter("@ProximoMantenimiento",cProximoMantenimiento.SelectionRange.Start),
                    //        new OleDbParameter("@HorasTrabajadas",txtHoras.Text), };
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = @"UPDATE Mantenimientos SET EquipoId = " + Convert.ToInt32(txtEquipoId.Text) + ", " +

                                                          "FiltroAceite = " + rbFiltroAceite1A.Checked + ", " +
                                                          "FiltroGasoil = " + rbFiltroGasoil1A.Checked + "," +
                                                          "FiltroAire = " + rbFiltroAire1A.Checked + ", " +
                                                          "FiltroAgua = " + rbFiltroAgua1A.Checked + ", " +
                                                          "Cooland = " + rbCooland1A.Checked + "," +
                                                          "Aceite = " + rbAceite1A.Checked + ", " +
                                                          "CorreaAlternador = " + rbCorreaAlternador1A.Checked + ", " +
                                                          "CorreaBombaAgua = " + rbCorreaBombaAgua1A.Checked + ", " +
                                                          "InfoAdicional = '" + txtInformacionAdicional.Text + "', " +
                                                          "ProximoMantenimiento = '" + cProximoMantenimiento.SelectionStart + "', " +
                                                          "HorasTrabajadas = " + txtHoras.Text + " " +
                                                          "WHERE ID =" + Convert.ToInt32(txtID.Text) + "";


                    cmd.ExecuteNonQuery();
                    con.Close();


                    MessageBox.Show("Registro Modificado");
                    limpiarCampos();
                    llenarGridPrincipal();
                   
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Error modificando los datos" + ex);
            }
        }

        private void llenarGridPrincipal()
        {
            var lista = helper.ExecuteQuery("SELECT * FROM vMantenimientos ", CommandType.Text, null).Tables[0];

            dtMantenimientos.DataSource = null;
            dtMantenimientos.DataSource = lista;
            tabMantenimiento.SelectedTab = tabListado;
        }

        private void dtMantenimientos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id = (int)dtMantenimientos.Rows[e.RowIndex].Cells[0].Value;

            llenarGridEquipo(id);

            btnEditarEquipo.Enabled = true;
            btnLimpiar.Enabled = true;
            btnCrearEquipo.Enabled = false;

        }

        public void llenarGridEquipo(int id)
        {
          

            rbFiltroAceite1A.Checked = false;
            rbFiltroGasoil1A.Checked = false;
            rbFiltroAire1A.Checked = false;
            rbFiltroAgua1A.Checked = false;
            rbCooland1A.Checked = false;
            rbAceite1A.Checked = false;
            rbCorreaAlternador1A.Checked = false;
            rbCorreaBombaAgua1A.Checked = false;
            DataTable consulta = helper.ExecuteQuery("SELECT * FROM Mantenimientos WHERE Id = " + id + " ", CommandType.Text, null).Tables[0];
            txtID.Text = consulta.Rows[0][0].ToString();
            txtEquipoId.Text = consulta.Rows[0][1].ToString();
            //Para llenar combo empresa y cliente
            var resultado = helper.ExecuteQuery("SELECT * FROM Equipos WHERE Id = " + Convert.ToInt32(txtEquipoId.Text) + "", CommandType.Text, null).Tables[0];
            cbCliente.SelectedValue = resultado.Rows[0][1];
            cbEquipo.SelectedValue = consulta.Rows[0].ItemArray[1];
            lbFecha.Text = consulta.Rows[0][2].ToString();
            //Nota: investigar como hacer if de una sola linea. Algo con el operador ternario?
            //Region de IF donde vaido los check de los radiosbutton
            
            #region
            if (Convert.ToBoolean(consulta.Rows[0].ItemArray[3].ToString()) == true)
            {rbFiltroAceite1A.Checked = true;}
            else
            {rbFiltroAceite1B.Checked = true;}

            if (Convert.ToBoolean(consulta.Rows[0].ItemArray[4].ToString()) == true)
            {rbFiltroGasoil1A.Checked = true;}
            else
            {rbFiltroGasoil1B.Checked = true;}

            if (Convert.ToBoolean(consulta.Rows[0].ItemArray[5].ToString()) == true)
            { rbFiltroAire1A.Checked = true; }
            else { rbFiltroAire1B.Checked = true; }

            if (Convert.ToBoolean(consulta.Rows[0].ItemArray[6].ToString()) == true)
            { rbFiltroAgua1A.Checked = true; }
            else { rbFiltroAgua1B.Checked = true; }

            if (Convert.ToBoolean(consulta.Rows[0].ItemArray[7].ToString()) == true)
            { rbCooland1A.Checked = true; }
            else { rbCooland1B.Checked = true; }

            if (Convert.ToBoolean(consulta.Rows[0].ItemArray[8].ToString()) == true)
            { rbAceite1A.Checked = true; }
            else { rbAceite1B.Checked = true; }

            if (Convert.ToBoolean(consulta.Rows[0].ItemArray[9].ToString()) == true)
            { rbCorreaAlternador1A.Checked = true; }
            else { rbCorreaAlternador1B.Checked = true; }

            if (Convert.ToBoolean(consulta.Rows[0].ItemArray[10].ToString()) == true)
            { rbCorreaBombaAgua1A.Checked = true; }
            else { rbCorreaBombaAgua1B.Checked = true; }
            #endregion



            txtInformacionAdicional.Text = consulta.Rows[0].ItemArray[11].ToString();
            cProximoMantenimiento.SetDate(Convert.ToDateTime(consulta.Rows[0].ItemArray[12]));
            txtHoras.Text = consulta.Rows[0].ItemArray[13].ToString(); ;
            tabMantenimiento.SelectedTab = tabCrear;
            if (txtID.Text != "")
            {
                btnCrearEquipo.Enabled = false;
                btnEditarEquipo.Enabled = true;
            }

            //Para llenar el grid con los datos del equipo que se esta dando mantenimiento
            var r = helper.ExecuteQuery("SELECT Descripcion, Serial, Marca, Motor, RPM, HP, Generador, KW, Voltios, Amperes, Hz, Conexion " +
                                        "FROM Equipos WHERE Id = " + Convert.ToInt32(txtEquipoId.Text) + "", CommandType.Text, null).Tables[0];
            dtEquipo.DataSource = null;
            dtEquipo.DataSource = r;
        }

        private void cbCliente_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            string id = cbCliente.SelectedValue.ToString();
            llenarComboEquipo(id);
        }
    }

}
